from flask import Blueprint, jsonify

api = Blueprint('api', __name__)

@api.route("/api/")
def test():
	return jsonify({'Hello':'World'})
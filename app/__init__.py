from flask import Flask 

def create_app():
	app = Flask(__name__)

	from app.main.routes import main 
	from app.api.routes import api
	from app.pwa.routes import pwa
	
	app.register_blueprint(main)
	app.register_blueprint(api)
	app.register_blueprint(pwa)

	return app
# Flaskeleton

The bare minimum required to float a Flask app using the blueprints/app factory pattern. PWA support built-in.

## Default blueprints
Flaskeleton comes with three registered blueprints:

- `Main`: Returns a simple Hello World template. Intended for server-side rendered assets.
- `API`: Returns a Hello World JSON blob. Intended for data fetched via AJAX.
- `PWA`: The bare minimum required to float a Progressive Web Application by default.

## Web assets

Flaskeleton comes with minimal web assets. It includes a lone HTML template, `main.html`, to render a Hello World page. No CSS is included.

All other static assets (`js/app.js`, `sw.js`, `manifest.json`, and the contents of `images/icons/`) are for PWA support. These files contain only the bare minimum required for Chrome-based browsers to initiate the *Install* prompt, so feel free to modify them to suit your app's PWA strategy (caching strategies, splash pages, custom install prompts, etc). 

A default icon set is included in `images/icons/` so the PWA functions but you'll probably want to change this to suit your app's branding. Sites like [Real Favicon Generator](https://realfavicongenerator.net/) can resize your logo to the common icon sizes defined in `manifest.json`.

## Dependencies
Since most Flask projects end up installing the same packages for things like forms, database ORMs, and user management, the following are included by default in `requirements.txt`:

- Flask-SQLAlchemy
- Flask-WTF
- Flask-Login

Feel free to delete them from the requirements file before installation if you don't need them (i.e. you're using JWT authentication instead of session-based logins)
